
///////Object Dstructure
const user = {
  id: 1,
  displayName: {
    shreyash: "shreyash",
    debrath: "debrath",
  },
  fullName: {
    firstname: "shreyash",
    lastname: "shrestha",
  },
};

//////which one is better practice

// const userFunt = ({displayName : shreyash})=>{
//     return shreyash;
// }

// const userFun = ({displayName})=>{
//     return displayName;
// }

// console.log(userFunt(user));

// const userFunction = ({displayName : uname, fullName : {firstname : fname}})=>{
//     return `${uname} is ${fname}`
// }

// console.log(userFunction(user));

//for each loop

// const array1 = ['a', 'b', 'c'];

// array1.forEach(element => console.log(element));

// const foreachLoop = [{shreyash : 'shreyash', debrath : "sharma", aagya : "aryal", amisha : "rijal", kripa : 'pokhrel'}, {fname : "shreyash", lname : "shrestha"}];
// foreachLoop.forEach((element , index, array) => {
//     // console.log(element);
//     // console.log(imdex);
//     // console.log(array);
// })

////// some() loop

// const someEle = [1,,3,,5]

// const odd = (el) => {
//     return el % 2 == 0;
// }

// console.log(someEle.some(odd));

/////// fine() loop

// const foundDogs = [
//     {
//         breed : 'debrath',
//         color : 'brown'
//     },
//     {
//         brees : 'shreyash',
//         color : 'grey'
//     },
//     {
//         breed : 'sagar',
//         color : 'black'
//     }
// ]

// function findMyDog(dog){
//     return dog.breed === "debrath";
// }

// const myDog = foundDogs.find(dog => findMyDog(dog));

// console.log(myDog);

// const array1 = [5, 12, 8, 130, 44];

// const found = array1.find(element => element > 10);

// console.log(found);

////// map() Loop

// Arrow function
// map((element) => { /* ... */ })
// map((element, index) => { /* ... */ })
// map((element, index, array) => { /* ... */ })

// const arr = [2, 4, 6, 8, 10];
// console.log(arr.map(arr => arr * 2));

///// filter(0)loop

// const words = ['shreyash', 'debrath', 'aagrya', 'amisha', 'kripa'];

// const filt = words.filter((word, ind, arr) => word.length > 6);

// console.log(filt);

////// reduce() loop

// p= 0 , c= 1 =>  p = 1
//p = 1 , c = 2 => p = 3
//p = 3, c = 3 => p = 6
// p 6, c= 4 => p = 10

// const redu_arr = [1,2,3,4];
// const initialVal = 0;
// const redu_opr = redu_arr.reduce( (prevVal, currVal, ind, arr) => {
//     prevVal += currVal;
//     if(ind < redu_arr){
//         prevVal * redu_arr.length;
//     }else{
//         return prevVal
//     }
// }, initialVal);

// console.log(redu_opr);

////// array destructuring

// let a,b, rest;

// [a,b] = [1,2]

// console.log(a); //1
// console.log(b); //2

// [a,b, ... rest] = [1,2,3,4,5,6,7];

// console.log(rest); // [3,4,5,6,7]

// const x = [1, 2, 3, 4, 5];
// const [y, z] = x;
// console.log(y); // 1
// console.log(z); // 2

// const colors = ['yellow', 'blue', 'green', 'red'];

// [one, two, three, four] = colors

// console.log(one);
// console.log(two);
// console.log(three);
// console.log(four);

// /// swaping variable

// let avo = 1;
// let bow = 3;
// [avo, bow] = [bow, avo]

// console.log(avo);//3

////// HOF function
// takes a fucntion as a parameter or return as a function

// function some(array, callback){
//     let result = array.reduce((acc, iterator) => {
//       return callback(iterator)
//     }, false)
//     return result
//  }

// Implementation
// first class citizen -> fn as a parameter
// hof

//  const testCase = (iterator) => {
//    return iterator === 3;
//  }

//  console.log(some([1,4,3], testCase))

//  function find()

///// function constructor //////

// const sutdnet1 = {
//   firstName: "shreyash",
//   lastName: "shrestha",
//   age: 22,
//   class: 4,
// };

// const student = (fname, lname, age, clas) => {
//     this.firstName = fname;
//     this.lastName = lname;
//     this.age = age;
//     this.class = clas;
// }

////  construction funciton is created inside a object

// function Student(fname, lname, age, cls, nationality) {
//   this.firstName = fname;
//   this.lastName = lname;
//   this.age = age;
//   this.class = cls;
//   this.nationality = nationality;
//   this.name = () => {
//     return (
//       this.firstName + " " + this.lastName + "is a" + " " + this.nationality
//     );
//   };
// }

//// object construction is contructed here

// const student1 = new Student("shreyash", "shrestha", 21, 4, "nepali");
// const student2 = new Student("shreyash", "shrestha", 21, 4, "australian");

// student1.nationality = "nepali";

//// funciton is called over here

// console.log(student1.name());
// console.log(student2.name());

/////// factory function ////////

// var persons = (fname, lname, work_place)=>{

//     //object is created

//     let person = {};

//     // parameter is set as a object key

//     person.fname = fname;
//     person.lname = lname;
//     person.work_place = work_place;

//     person.printPersonDetail = ()=>{
//       return person.fname + " " + person.lname;
//     }

//     return person;
//   }

//     // created instance of a funcion

//     const person1 = new persons("shreyash", "shrestha", "hazesoft");

//     console.log(persons.printPersonDetail());

///// spread operators

///////// the wy to using spread opr to the number operation

// const spread_opr = [12,1,2,3];
// const sum = (x,y,z) => {
//   return x + y + z;
// }

// // console.log(sum(...spread_opr));
// // console.log(sum(...spread_opr));

// //////// the way of adding/ storing  data with the help of spread operators
// let num = [1,2,3];
// let new_num = [...num, 4];
// // console.log(new_num);

// let prtFun = (x,y,z)=>{
//   return x+y+z;
// }
// let arr1 = [1,2,3,4];
// console.log(prtFun(...arr1));

/////// closer in js

// function shreyash(shre){
//   return (str)=>{
//     console.log(shre);
//     console.log(str);
//   }
// }
// // console.log(shreyash("shreyash"));
// let shrestha = shreyash('shreyash');
// shrestha('shrestha');

///////// puttting the private methods on closers

// let counter_value = (function () {
//   let private_counter = 0;
//   function change_val(val) {
//     private_counter += val;
//   }

//   return {
//     increment: function () {
//       change_val(2);
//     },
//     decrement: function () {
//       change_val(-2);
//     },
//     value: function () {
//       return private_counter;
//     },
//   };
// })();

// counter_value.increment();
// counter_value.increment();
// console.log(counter_value.value());

// counter_value.decrement();
// console.log(counter_value.value());

// let count_num = (function(){
//   initial_val = 0;
//   function chang_val(val){
//     initial_val += val;
//   }
//   return {
//     increment : ()=>{
//       chang_val(2);
//     },
//     decrement : ()=>{
//       chang_val(-2);
//     },
//     value : ()=>{
//       return initial_val;
//     }
//   }
// })();

// count_num.increment();
// console.log(count_num.value());

//////// work with Ajax and http req ( Async and sync request)

// function onRenderData() {
//   console.log("this is working");
//   ////// create a object for a xmlhttprequest

//   const xhttp = new XMLHttpRequest();

//   ////// define call back function for the response time execution

//   xhttp.onload = () => {
//     document.getElementById("ajax_req").innerHTML = 
//     this.responseText;
//   };

//   /////// send a http request to a sever

//   xhttp.open("GET", 'objdis.txt');
//   xhttp.send();
// }



////////    objetc.key()


const obj = Object.create({}, {
  getFoo : {
    value : ()=>{
      return this.foo;
    }
  }
});

obj.hoo = {};
// const obj1 = {1:'shreyash', 2:'amisha', 3:'debrath'}

// console.log(Object.keys(obj.shreyash));

console.log(Object.keys(obj))

/**
 * 1. Write a function to get the sum of first n natural numbers. (recursive)
 * 2. Write a function to get a factorial of a given number,
 * 3. Write a function to get the fibonacci number.
 * 4. Write a function to get the sum of first n natural even numbers.
 * 5. Given [1, 2, null, undefined, {} , []], write a program to get the sum of valid number.
 * 6. Write a generic function greeting which takes in a callback and executes that callback with a message.
 * 7. Given an two object A and B, Write a function to check if these two objects are equal.
 * 8. Write a function to test if given two array are equal.
 * 9. Write a function to Create you own data structure for stack.
 * 10. Write a function to create your own data structure for queue.
 * 11. Write a curry function sum that adds two numbers which returns another function and can be partially applied.
 * Example:
 *  const addTo2 = add(2)
 *  const addTo3 = add(3)
 *  console.log(addTo2(3)) // 5
 *  console.log(addTo3(1)) // 4
 * 12. Write a function that converts a given function into curry function.
 * 13. Write a function to multiply the marks by 0.5 of given students that have marks higher than 80. Also, the resulting array should 
 * only contains students that have marks with higher than 80.
 *  Given students = [
 *  { name: "sth", marks: 82 },
 *  { name: "sth", marks: 90},
 *  { name: "sth", marks: 78 },
 *  { name: "sth", marks: 87 },
 *  { name: "sth", marks: 82 },
 *  { name: "sth", marks: 88 },
 * ]
la ramrari gar hai ta haha



**/
