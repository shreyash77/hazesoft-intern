# Hazesoft-intern

1. Informational responses (100–199)
2. Successful responses (200–299)
3. Redirection messages (300–399)
4. Client error responses (400–499)
5. Server error responses (500–599)


1. 100 Continue
This interim response indicates that the client should continue the request or ignore the response if the request is already finished.

2. 101 Switching Protocols
This code is sent in response to an Upgrade request header from the client and indicates the protocol the server is switching to.

3. 102 Processing (WebDAV)
This code indicates that the server has received and is processing the request, but no response is available yet.

