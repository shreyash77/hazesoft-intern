const express = require("express");
const path = require("path");
const app = express();
const port = 8000;
const Handlebars = require("hbs");
const fs = require("fs");
const { create, ExpressHandlebars } = require("express-handlebars");
const morgan = require("morgan");
const { executionAsyncResource } = require("async_hooks");

// use of morgan

app.use(morgan("tiny"));
// use of custome helper

// Handlebars.registerHelper('loud', function(astring){
//     return astring.toUpperCase();
// });

// Handlebars.registerHelper('test', () => {
//     console.log(this, "instance")
//     return null
// })

// custome helper using express-handlebars

// const hbs  = create({
//     helpers : {
//         foo(){return "fool";},
//         bar(){return "bar";}
//     }
// })

Handlebars.registerHelper("print_person", () => {
  // console.log(this, "test")
  return this.firstName + " " + this.lastName;
});

// to set the view engine

// app.engine('hbs', hbs.engine);

app.set("view engine", "hbs");

app.set("views", "./views");

app.get("/", (req, res) => {
  res.redirect("home");
});

app.get("/404", function (req, res) {
  res.status(404).send("Status: Not Found");
});

app.get("/home", (req, res) => {
  res.render("home", {
    pname: true,
    people: ["Yehuda Katz", "Alan Johnson", "Charles Jolley"],
    pop: [
      {
        firstName: "namaste",
        lastName: "london",
      },
      {
        firstName: "debrath",
        lastName: "sharma",
      },
    ],
    firstName: "shreyash",
    lastName: "shrestha",
    peoples: {
      firstname: "debrath",
      lastname: "sharma",
    },
    city: {
      name: "kathmandu",
      summary: "Ktm is a beautiful city",
      location: {
        north: "22.455.6",
        south: "33.55.77",
      },
    },
    title: "home",
    intro: "Your are in home page",
  });
});

// for about page

app.get("/about", (req, res) => {
  res.send(req.params);
  res.render("about", {
    title: "About",
    intro: "Your are in about page",
  });
});

//for contact page

app.get("/contact", (req, res) => {
  res.render("contact", {
    title: "contact",
    intro: "Your are in contact page",
  });
});

//for 404 page

// app.get("/404", (req, res) => {
//   res.render("404", {
//     title: "404",
//     intro: "404, OOPSSSS page not found",
//   });
// });

app.use((req, res) => {
  res.status(404).render('404', {
    title : '404',
    intro : "404, OOPSSS page not found"
  })
})

app.get("/user/profile/:id", (req, res) => {
    const {id} =  req.params
    const intID = parseInt(id)

  const users = [
    {
      id: 2,
      name: "debrath",
    },

    {
      id: 2,
      name: "Shreyash",
    },
  ];

  const found = users.find(x => x.id === intID)
  if(!found){
    return res.status(404).send("Not found")
  }
  
  const {name} = found
  const template = `hi ${name}`
  res.status(200).send(template)
});

// make css public using express js

app.use(express.static('public'));

// server hit

// server.listen(port, ()=>{
//     console.log(`listining to  the port ${port}`);
// })

app.listen(port, () => {
  console.log(`Listen to the port ${port}`);
});
