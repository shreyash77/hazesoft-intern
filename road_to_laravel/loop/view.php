<!DOCTYPE html>
 <html lang="en">
 <head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Document</title>
 </head>
 <body>
     <ul>

        <!-- basic array concept -->

        <!-- <?php 
            foreach($names as $name){
                echo "<li> $name </li>";
            }
        ?> -->

        <!-- associative array concept -->
        
        <!-- <?php 
            foreach($person as $key => $val){
                echo "<li> <strong>$key: </strong> $val </li>";
            };
        ?> -->



    <!-- <ul>
        <?php  foreach($tasks as $key => $val) : ?>
            <li>
                <strong> <?=  ucWords($key) ?>: </strong> <?= $val ?>
            </li>
            <?php endforeach; ?>
        </ul> -->
        
    </ul>

    <h2>Task For The Day!</h2>

    <ul>
        <li>
            <strong>Name:</strong> <?= $tasks["title"]; ?>
        </li>
        <li>
            <strong>Due Date:</strong> <?= $tasks["due"]; ?>
        </li>
        <li>
            <strong>Personal Responsibility:</strong> <?= $tasks["assign_to"]; ?>
        </li>
        <li>
            <!-- #### HERE COMES THE TERNAY OPERATORS WHICH WILL TAKE A CONDITION AND GIVES THE VALUE -->
            <strong>Status: </strong> <?= $tasks["complete"] ? '&#9989' : '&#10060' ?>
        </li>
    </ul>


    <!-- //**  To get the details of the car  ** */ --> 
    <!-- //**** with complete palying with array and loop ie foreach loop */ -->
    <div>
        <?php
            foreach($car as $key => $val){  
                echo "<p><b>" ."Row Number"." ". $key+1 . "</b><p>";

                echo "<ul>";

                    foreach($val as $value){
                        // echo "<li>". $value[$val_key][$key]. "</li>";
                        echo "<li> <strong>" . $value. "</strong></li>";
                    }
                
                echo "</ul>";
            }
        ?>
    </div>
 </body>
 </html>



