<?php

####### for $GLOBAL[] methods

// $a = 10;
// $b = 10;

// function addValue()
// {
//     echo $GLOBALS['a'] + $GLOBALS['b'];
// }
// addValue();
?>


<!-- #### for $_GET[] Methods -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Vaidation</title>
    <style>
        form{
            /* margin: 20px;
            display: flex; */
            justify-content: left;
        }
        form label{
            margin-left : 20px;
        }
        .error{
            color: red;
        }
    </style>
</head>
<body>
    <!-- 

        Field	    Validation Rules
        Name    =>	Required. + Must only contain letters and whitespace
        E-mail	=>  Required. + Must contain a valid email address (with @ and .)
        Website	=>  Optional. If present, it must contain a valid URL
        Comment	=>  Optional. Multi-line input field (textarea)
        Gender	=>  Required. Must select one


     -->

    <?php

        $name = $age = $email = $contatNumber = $gender = "";
        $errName = $errAge = $errEmail = $errContatNumber = $errGender = "";




        if($_SERVER["REQUEST_METHOD"] == "POST"){
            if(empty($_POST["name"])){
                $errName = "*Name is required";
            }else{
                $name = testInput($_POST["name"]);
            }

            if(empty($_post["age"])){
                $errAge = "*Age is required";
            }else{
                $age = testInput($_POST["age"]);
            }

            if(empty($_POST['email'])){
                $errEmail = "*Email is required";
            }else{
                $email = testInput($_POST["email"]);
            }

            if(empty($_POST["number"])){
                $errContatNumber = "*Contact Number is required";
            }else{
                $contatNumber = testInput($_POST["number"]);
            }

            if(empty($_POST["gender"])){
                $errGender = "*Gender is required";
            }else{
                $gender = testInput($_POST["gender"]);
            }
        }

        ###### check for the input data whether it contains any special char or not 

        function testInput($data){
            $data = trim($data);
            $data = htmlspecialchars($data);
            $data = stripslashes($data);
            return $data;
        }


    ?>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) ?>">

        <label for="name">Name:</label>
        <input type="text" name="name" value="">
        <span class="error"><?= $errName ?></span>
        <br><br>
        <label for="age">Age:</label>
        <input type="text" name="age" value="">
        <span class="error"><?= $errAge ?></span>
        <br><br>
        <label for="email">Email:</label>
        <input type="email" name="email" value="">
        <span class="error"><?= $errEmail ?></span>
        <br><br>
        <label for="number" >Contact Number</label>
        <input type="text" name="number" value="">
        <span class="error"><?= $errContatNumber ?></span>
        <br><br>
        <label for="gender">Gender:</label>
        <input type="radio" name="gender" value="female" id=""> Female
        <input type="radio" name="gender" value="female" id=""> male
        <input type="radio" name="gender" value="female" id=""> other
        <span class="error"><?= $errGender ?></span>
        <br> <br>
        <input type="submit" name="submit" value="submit">

    </form>

</body>
</html>










