<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Practice</title>
    <style>
        body {
            background-color: #534292;
        }
        
        h3 {
            color: #736e6e;
        }
        
        .heading {
            text-align: center;
        }
        
        .container {
            position: relative;
            left: 50%;
            top: 50%;
            padding: 30px;
            background-color: white;
            width: max-content;
            border-radius: 5px;
            transform: translate(-50%, 50%);
        }
        
        .auth {
            width: 90%;
            padding: 15px;
            background-color: rgb(234, 234, 234);
            border: none;
            margin-bottom: 10px;
            border-radius: 4px;
            color: #75879b;
        }
        
        .auth:focus {
            outline: none;
            /* border: none !important; */
        }
        
        .checkbox {
            margin-bottom: 25px;
        }
        
        button {
            width: 97%;
            margin-bottom: 15px;
            background-color: #8064e2;
            padding: 12px;
            border: none;
            border-radius: 4px;
            color: whitesmoke;
            font-weight: 700;
        }
        
        button:hover {
            background-color: #5941af;
        }
        
        label {
            font-size: 10px;
        }
        
        a {
            font-size: 15px;
            text-decoration: none;
            color: #808080;
        }
        
        .anchor {
            text-align: center;
        }
    </style>
</head>

<body>
    <div class="container">

        <div class="heading">
            <h3>SIGN IN TO YOUR ACCOUNT</h3>
        </div>

        <input class="auth" type="email" placeholder="joshi.shreyash1@gmail.com">

        <input class="auth" type="password" placeholder="********"> <br>

        <input class="checkbox" type="checkbox" name="" id="">
        <label>Keep me sign in</label> <br>

        <button type="submit">SIGN IN</button>
        <br>
        <div class="anchor">
            <a href="#">Forgot your password? </a>
        </div>
    </div>
    <!-- <div>
        <h2>JavaScript Gelocation</h2>

        <p>Click the button to get your coordinates.</p>

        <button onclick="getLocation()">Try It</button>

        <p id="demo"></p>
    </div> -->
</body>

</html>