<?php 

    ######## LIBRARY FUNCTION FOR STRING PHP

    ### substr() //** It  is used to get the sub-string between the last and the end of the given string **/ 
    ### sub(string, offset, length);

    // $str = "I'm Shreyash Shrestha";
    // $result = substr($str, -3, 3);
    // echo $result;


    ### strlen(); // ** It's used to give the length of the string **//
    ### strlen(string);

    // $str = "My name is Shreyash Shrestha";
    // $result = strlen($str);
    // echo $result;


    ### str_replace() //** It's used to search and replace the value to the given string on the funtion **/
    ### str-replace(search, replace, string); // which take 3 parameter for the replacement of the string;


    // $phrase  = "You should eat fruits, vegetables, and fiber every day.";
    // $healthy = ["fruits", "vegetables", "fiber"];
    // $yummy   = ["pizza", "beer", "ice cream"];  

    // $newphrase = str_replace($healthy, $yummy, $phrase);
    // echo $newphrase;


    ### trim() //** It's help to trip the whitespace in the string or code and takes the value to trip in the string **//

    ### trip(string); // we have left trim for ltrim() and right trim for rtrim()

    // $str = "\@%WTF     Can this world be changed by me \@%";
    // echo trim($str, "/\@%");



    ### strpos(string, position, offset)
    ### strpos() //** It tells the position of the asked string bby default it will search from the left if the off set is given then it will search according to the offset **//

    // $myStr = "Shreyash is good Boy";

    // function findMyPosition($str){
    //     $findMe = "good";
    //     $result = strpos($str, $findMe);
    //     if($result == false){
    //         return "Not found";
    //     }else{
    //         return "Found at postion" . " " . $result;
    //     };
    // }

    // echo findMyPosition($myStr);



    ### strtolower() //** it change the string to the lower case if it is in upper case **//
    ### strtolower(string);
    
    // $myStr = "World Can BE CHANGed BY uS";
    
    // function makeStrLower($str){
    //     return strtolower($str);
    // }

    // echo makeStrLower($myStr) . "<br>";



    
    ### strtoupper() //** it change the string to the upper case if it is in lower case **//
    ### strtoupper(string);

    // function makeStrUpper($str){
    //     return strtoupper($str);
    // }
    
    // echo makeStrUpper($myStr);


    ### is_string() //** It's used to check whether given date is string or not // it return the truth and false value in return **/
    ### is_string(string);

    // $myData = 8;

    // function checkStringOrNot($val){
    //     if(is_string($val)){
    //         return "true";
    //     }else{
    //         return "false";
    //     }
    // }

    // echo checkStringOrNot($myData);




    ### strstr() //** It will search the string which is saked to be searched and give to user **/
    ### strstr(string, search, boolean);

    // $myStr = "shreyash is stupid";
    // echo strstr($myStr, "is");


    // function CheckPosition($str){
    //     if(strstr($str, "good") == false){
    //         return $str;
    //     }
    //     else{
    //         return "fasle";
    //     }
    // }

    // echo CheckPosition("he's a good boy");



 
    ### strcmp() //** used to compare two string **/
    ### strcmp(string1, string2);
    //! 0 – if the two strings are equal
    //! <0 – if string1 is less than string2
    //! >0 – if string1 is greater than string2
    
    // $myStr1 = "this is shreyash";
    // $myStr2 = "this is shreyash";
    // echo  strcmp($myStr1, $myStr2); 



    ### explode() //** It's convert string into array on the basis of delimiter passed**/
    ### explode(delimiter, string);


    // $myStr = "Coding has changed the world of technology";
    // echo "<pre>";
    // print_r( explode(" ", $myStr));
    // echo "</pre>";



    ### impload() //** It's used to convert array to string on the basis of delimiter passed **/
    ### impload(delimiter, array[]);

    // $myArray = ["coding", "has", "changed", "the", "world", "of", "technology"];
    // echo implode("#", $myArray);


















?>